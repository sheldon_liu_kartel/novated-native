import Close from './Icons/Close';

import VehicleList from './Icons/VehicleList';

export default {
  close: Close,
  'vehicle-list': VehicleList
}
