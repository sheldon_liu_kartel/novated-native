import React from 'react';

import {G, Polygon} from 'react-native-svg';

const Close = ({fill}) => (
  <G>
    <Polygon fill={fill} points="502 50 452 0 252 200 52 0 2 50 202 250 2 450 52 500 252 300 452 500 502 450 302 250" />
  </G>
);

export default Close;
