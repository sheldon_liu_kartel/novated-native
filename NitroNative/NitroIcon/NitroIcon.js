import React from 'react';
import PropTypes from 'prop-types';

import { View } from 'react-native';

import styled, { withTheme } from 'styled-components/native';
import Svg from 'react-native-svg';

import iconList from './iconList';

// import { IconContainer } from './styles';

const IconContainer = styled.View`
  width: ${props => props.size};
  margin-left: ${props => props.left || 0};
  margin-right: ${props => props.right || 0};
  margin-top: ${props => props.top || 0};
  margin-bottom: ${props => props.bottom || 0};
`;

const NitroIcon = ({ name, size, variant, left, right, top, bottom, action, theme }) => {
  const Icon = iconList[name];
  // const cursor = action || clickable ? 'pointer' : 'inherit';
  const fill = theme.color[variant];
  return (
    <IconContainer
      size={size}
      left={left}
      right={right}
      top={top}
      bottom={bottom}
      onClick={action}>
      {Icon &&
        <Svg width={size} height={size} viewBox="0 0 500 500">
          <Icon fill={fill} />
        </Svg>}
    </IconContainer>
  );
};

NitroIcon.propTypes = {
  name: PropTypes.string,
  size: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  variant: PropTypes.string,
  left: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  right: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  top: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  bottom: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  action: PropTypes.func,
  clickable: PropTypes.bool,
  alignment: PropTypes.bool,
  hoverVariant: PropTypes.string,
  shadow: PropTypes.bool,
  currentColor: PropTypes.bool,
  className: PropTypes.string,
  pointerEvents: PropTypes.string
};

NitroIcon.defaultProps = {
  size: 16,
  variant: 'base2'
};

export default withTheme(NitroIcon);
