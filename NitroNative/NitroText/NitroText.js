import React from 'react';
import PropTypes from 'prop-types';

import { Text } from 'react-native';

import styled from 'styled-components/native';

const TextWrapper = styled.Text`
  font-size: ${props => props.theme.size[props.size] || props.size};
  font-weight: ${props => (props.bold ? 'bold' : 'default')};
  color: ${props => props.theme.color[props.variant]};
`;

const NitroText = ({ children, size, bold, variant }) => {
  return (
    <TextWrapper size={size} bold={bold} variant={variant}>
      {children}
    </TextWrapper>
  );
};

NitroText.propTypes = {};

NitroText.defaultProps = {
  size: '4',
  variant: 'base2',
  bold: false
};

export default NitroText;
