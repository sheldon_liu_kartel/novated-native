import React, { Component } from 'react';
import { Modal, Text, TouchableHighlight, View, Platform } from 'react-native';

import styled from 'styled-components/native';

import NitroIcon from '../NitroIcon';

const DrawerContent = styled.View`
  flex: 1;
  background-color: ${props => props.theme.color[props.variant] || '#707070'};
  padding-top: ${props => (Platform.OS === 'ios' ? 20 : 0)};
`;

const CloseButton = styled.View`
  position: absolute;
  z-index: 10;
  right: 12;
  top: 4;
  height: 24;
  width: 24;
  align-items: center;
  justify-content: center;
`;

class NitroDrawer extends Component {
  render() {
    const { isOpen, close, padding, variant } = this.props;
    return (
      <Modal animationType="slide" transparent={false} visible={isOpen} onRequestClose={close}>
        <DrawerContent padding={padding} variant={variant}>
          <TouchableHighlight onPress={() => close()}>
            <CloseButton>
              <NitroIcon name="close" variant="white" size={12} />
            </CloseButton>
          </TouchableHighlight>

          {this.props.children}
        </DrawerContent>
      </Modal>
    );
  }
}

export default NitroDrawer;
