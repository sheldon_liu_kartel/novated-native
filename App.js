import React from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native';

import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';

import { NativeRouter, Route, Link, Switch } from 'react-router-native';
import styled from 'styled-components/native';

import theme from './src/themes';
import configureStore from './src/store';
import AppRoutes from './src/routes';

export default class App extends React.Component {
  render() {
    const store = configureStore();
    return (
      <Provider store={store}>
        <ThemeProvider theme={theme}>
          <NativeRouter>
            <AppRoutes />
          </NativeRouter>
        </ThemeProvider>
      </Provider>
    );
  }
}
