import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import VehicleView from './VehicleView';

import { actions, selector } from '../../store/global/vehicles/index';

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(actions, dispatch)
});

const mapStateToProps = state => ({
  vehicleList: selector.getVehicleList(state),
  isLoading: state.vehicles.isFetchingVehicleList
});

export default connect(mapStateToProps, mapDispatchToProps)(VehicleView);
