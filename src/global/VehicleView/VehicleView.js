import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Switch, Route } from 'react-router-native';

// import { NitroLoading } from 'nitro-components';

import { ActivityIndicator, View } from 'react-native';

import styled from 'styled-components/native';
const Container = styled.View`flex: 1;`;

export default class VehicleView extends Component {
  componentWillMount() {
    // No vehicle fetch vehicle first
    if (this.props.vehicleList.length === 0) {
      this.props.actions.fetchVehicleList();
    }
  }

  render() {
    return this.props.isLoading
      ? <ActivityIndicator color="#0098C0" />
      : <Container>
          {this.props.children}
        </Container>;
  }
}

VehicleView.propTypes = {
  vehicleList: PropTypes.array,
  actions: PropTypes.object,
  isLoading: PropTypes.bool
};
