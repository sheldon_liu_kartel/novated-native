import React from 'react';
import { View } from 'react-native';
import styled from 'styled-components/native';
// import media from 'utils/media';

// import tinycolor from 'tinycolor2';

export const VehicleSwitchButton = styled.View`
  border-radius: 2px;
  background-color: ${props => props.theme.color.secondary};

  width: 66px;
  height: 22px;

  flex-direction: row;
  justify-content: space-around;
  align-items: center;
`;

export const DrawerHeading = styled.View`
  margin: 10px 26px;
  max-width: 300px;
`;

export const Divider = styled.View`
  height: 1px;
  width: 100%;
  background-color: rgba(0, 0, 0, 0.15);
`;
