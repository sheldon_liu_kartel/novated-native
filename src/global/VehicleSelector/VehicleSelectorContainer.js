import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import VehicleSelector from './VehicleSelector';

import { actions, selector } from '../../store/global/vehicles';

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(actions, dispatch)
});

const mapStateToProps = state => ({
  vehicleList: selector.getVehicleList(state),
  vehicle: selector.getSelectedVehicle(state)
});

export default connect(mapStateToProps, mapDispatchToProps)(VehicleSelector);
