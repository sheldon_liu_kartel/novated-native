import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { TouchableHighlight } from 'react-native';

// import { NitroDrawer, NitroText, NitroIcon } from 'nitro-components';
import { NitroText, NitroIcon, NitroDrawer } from '../../../NitroNative';
import styled from 'styled-components/native';

import { VehicleSwitchButton, DrawerHeading, Divider } from './styled';

const Highlight = styled.TouchableHighlight`border-radius: 2px;`;
// import VehicleInfo from 'pages/Dashboard/Dashboard/components/VehicleInfo';

// const RegoSize = {
//   default: '5',
//   desktop: '4',
//   mobile: 8
// };
// const VehicleIconSize = {
//   tablet: 15,
//   desktop: 16,
//   mobile: 10
// };

class VehicleSelector extends Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     displayList: false
  //   };
  // }
  //

  state = {
    displayList: false
  };

  open = () => {
    this.setState({
      displayList: true
    });
  };

  close = () => {
    this.setState({
      displayList: false
    });
  };

  handleSelect = vehicle => {
    this.props.actions.selectVehicle(vehicle);
    this.close();
  };

  render() {
    const { vehicleList, vehicle, display } = this.props;

    if (vehicleList.length < 2) {
      return null;
    }

    // const vehicleListSection = vehicleList.map(vehicle =>
    //   <div key={`vehicle-${vehicle.leaseId}`} onClick={() => this.handleSelect(vehicle)}>
    //     <VehicleInfo
    //       isDesktop={display === 'desktop'}
    //       isSelectable
    //       isHighLighted={this.props.vehicle.leaseId === vehicle.leaseId}
    //       vehicle={vehicle}
    //     />
    //     <Divider />
    //   </div>
    // );

    return (
      <Highlight onPress={this.open} activeOpacity={0.8}>
        <VehicleSwitchButton>
          <NitroText variant="white" size={8} bold>
            {vehicle.registration}
          </NitroText>
          <NitroIcon name="vehicle-list" size={10} variant="support1" />
          <NitroDrawer isOpen={this.state.displayList} close={this.close} variant="secondary">
            <DrawerHeading>
              <NitroText size="4" variant="white" bold>
                Choose one of your lease
              </NitroText>
            </DrawerHeading>
          </NitroDrawer>
        </VehicleSwitchButton>
      </Highlight>
    );

    // return (
    //
    //     <VehicleSwitchButton onClick={this.open}>
    //       <NitroText variant="white" size={RegoSize} bold>
    //         {vehicle.registration}
    //       </NitroText>
    //       {/* <NitroIcon
    //         name="vehicle-list"
    //         size={VehicleIconSize[display]}
    //         variant="support1"
    //         alignment={display === 'mobile'}
    //       /> */}
    //     </VehicleSwitchButton>
    //     {/* <NitroDrawer
    //       variant="support3"
    //       iconVariant="support1"
    //       isOpen={this.state.displayList}
    //       closeIconSize={{
    //         default: 12,
    //         desktop: 16
    //       }}
    //       close={this.close}
    //       padding="10px 0">
    //       <DrawerHeading>
    //         <NitroText size={{ default: '4', desktop: '2' }} variant="white" bold>
    //           Choose one of your lease
    //         </NitroText>
    //       </DrawerHeading>
    //       <Divider />
    //       {vehicleListSection}
    //     </NitroDrawer> */}
    //
    // );
  }
}

VehicleSelector.propTypes = {
  vehicle: PropTypes.object,
  vehicleList: PropTypes.array,
  actions: PropTypes.object,
  display: PropTypes.string
};

export default VehicleSelector;
