import { connect } from 'react-redux';
import WelcomeHeader from './WelcomeHeader';

const mapStateToProps = state => ({
  user: state.auth.user
});

export default connect(mapStateToProps)(WelcomeHeader);
