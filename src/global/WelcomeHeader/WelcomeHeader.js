import React from 'react';
import PropTypes from 'prop-types';

// Should be put into nitro-components
// import logo from './logo-fleet-partners-mobile.svg';
// import { NitroText } from 'nitro-components';
import { NitroLogo } from '../../../NitroNative';


import { View, Text } from 'react-native';

import styled from 'styled-components/native';

import VehicleSelector from '../VehicleSelector';

const HeaderContainer = styled.View`
  z-index: 1000;
  height: 30;
  flex-direction: row;
  align-items: center;
  background-color: ${props => props.theme.color.white};

  ${'' /* Shadow replacement */} shadow-color: black;
  shadow-opacity: 0.15;
  shadow-offset: 1px 0px;
  shadow-radius: 2;
`;

const WelcomeMessage = styled.Text`
  font-size: ${props => props.theme.size['5']};
  font-weight: bold;

  color: ${props => props.theme.color.base2};
`;

const LogoWrapper = styled.View`
  margin-left: 10px;
  margin-right: 4px;
  height: 14px;
  width: 14px;
`;

const VehicleSwitchWrapper = styled.View`
  margin-right: 10px;
  margin-left: auto;
`;

const WelcomeHeader = ({ user, display }) => {
  return (
    <HeaderContainer>
      <LogoWrapper>
        <NitroLogo size="14" />
      </LogoWrapper>
      <WelcomeMessage>{`${user.firstName}, welcome to your Novated Portal`}</WelcomeMessage>

      <VehicleSwitchWrapper>
        <VehicleSelector display={display} />
      </VehicleSwitchWrapper>
    </HeaderContainer>
  );
};

WelcomeHeader.propTypes = {
  user: PropTypes.object
};

export default WelcomeHeader;
