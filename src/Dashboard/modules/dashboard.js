import axios from 'axios';
import { API_ROOT } from '../../config';

// ------------------------------------
// Constants
// ------------------------------------

const FETCH_VEHICLE = 'dashboard/FETCH_VEHICLE';

// ------------------------------------
// Apis
// ------------------------------------

const vehicles = [
  {
    Variance: 2289.0,
    VarianceType: 'underBudget',
    Term: 36,
    CurrentMonth: 18,
    Claims: [
      {
        ClaimDate: '2017-05-10T00:00:00',
        ClaimId: 'Toll',
        ClaimName: 'Roads and Maritime Services',
        Amount: 500.0,
        StatusNote: 'Receipts missing',
        Status: 3
      },
      {
        ClaimDate: '2017-04-10T00:00:00',
        ClaimId: 'Fuel',
        ClaimName: 'BP Lane Cove',
        Amount: 61.25,
        StatusNote: 'Processed',
        Status: 1
      }
    ],
    TotalActual: 22336.0,
    Budgeted: 24625.0,
    TotalBudget: 39000.0,
    LeaseId: 1,
    RegoNo: 'KDM123',
    Make: 'Tesla',
    Model: 'Model X P100D',
    Derivative: null,
    Colour: 'Green',
    FBTValue: null,
    VIN: null,
    BuildDate: '2018',
    ContractKMS: 20,
    RegistrationStatus: 'Registered',
    VehicleImageUrl:
      'api/novated/image/vehicle/YXdVMjJUM255ZzJpQ2M1cXZSRnJwSUw3WUw3bkpaZEY5Y1pPKzNjNFhxeDBlVlAvdjhDMHZOeW1vY29mUGExOGE0d2Ixb1VJZHNsOUdxaGZTbFBlb09qblpEUEgzYzhMMk5KRE05TDNPU0tFT3U5ZWNKc0NoM1d5dlFXM2RueTE1'
  },
  {
    Variance: 3622.0,
    VarianceType: 'overBudget',
    Term: 48,
    CurrentMonth: 20,
    Claims: [
      {
        ClaimDate: '2017-04-16T00:00:00',
        ClaimId: 'Toll',
        ClaimName: 'Roads and Maritime Services',
        Amount: 100.0,
        StatusNote: 'Receipts missing',
        Status: 3
      },
      {
        ClaimDate: '2017-05-02T00:00:00',
        ClaimId: 'Fuel',
        ClaimName: 'BP Darling Harbour',
        Amount: 140.36,
        StatusNote: 'Processed',
        Status: 1
      }
    ],
    TotalActual: 28247.0,
    Budgeted: 24625.0,
    TotalBudget: 45050.0,
    LeaseId: 2,
    RegoNo: 'KDM123',
    Make: 'Mazda',
    Model: 'Mazda3',
    Derivative: null,
    Colour: 'Gray',
    FBTValue: null,
    VIN: null,
    BuildDate: '2015',
    ContractKMS: 20,
    RegistrationStatus: 'Unregistered',
    VehicleImageUrl:
      'api/novated/image/vehicle/ZEtIM3dCNVBVaUZWUitDd0ZCN3hqaWRraExSVUZjamhxMzJMQ1BlRU5VWW9lSGY3bjJNQWUzM1NUK1Q0M3hlM3NmOUk2Vmw0Wkw2Y29kMkNxYk9RWXdlWDJHZkZaWEFub1VDT3N3Y2dYdDlTSHcyMXFKSk5YbXdXYktUUVk4c1Y1'
  }
];

const endpoints = {
  lease: leaseId => `${API_ROOT}/leases/${leaseId}/summary`
};

function DelayPromise(delay) {
  // return a function that accepts a single variable
  return function(data) {
    // this function returns a promise.
    return new Promise(function(resolve, reject) {
      setTimeout(function() {
        // a promise that is resolved after "delay" milliseconds with the data provided
        resolve(data);
      }, delay);
    });
  };
}

const formatClaim = claims =>
  claims.map(claim => {
    const { ClaimDate: date, ClaimId: title, ClaimName, Amount: value, StatusNote: label, Status: type } = claim;
    return {
      date,
      title,
      value,
      status: {
        type,
        label
      }
    };
  });

const formatVehicleDetial = vehicle => {
  const {
    LeaseId: leaseId,
    ActualLTD: used,
    BudgetLTD: lifeToDate,
    TermBudget: total,
    Variance: variance,
    VarianceType: config,
    CurrentMonth,
    Term,
    Claims
  } = vehicle;

  return {
    config,
    budget: {
      variance,
      leaseId,
      used,
      lifeToDate,
      total
    },
    lease: {
      total: Term,
      current: CurrentMonth
    },
    claims: Claims ? formatClaim(Claims) : []
  };
};

const Api = {
  getVehicleData: leaseId => {
    const promise = new Promise((resolve, reject) => {
      resolve({
        data: vehicles[leaseId - 1],
        status: 200,
        statusText: 'OK',
        headers: {}
      });
    });
    // return promise.then(DelayPromise(1800)).then(data => data.data);

    // return promise.then(DelayPromise(1500)).then(data => formatVehicleDetial(data.data));
    return axios.get(endpoints.lease(leaseId)).then(({ data }) => formatVehicleDetial(data.data));
  }
};

// ------------------------------------
// Actions
// ------------------------------------

export const fetchVehicle = leaseId => ({
  type: FETCH_VEHICLE,
  payload: Api.getVehicleData(leaseId)
});

export const actions = {
  fetchVehicle
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  config: 'initial',
  isFetchingVehicle: false,
  budget: {},
  lease: {
    current: 0,
    total: 12
  },
  claims: []
};
const dashboardReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case `${FETCH_VEHICLE}_PENDING`:
      return Object.assign({}, state, {
        isFetchingVehicle: true
      });
    case `${FETCH_VEHICLE}_REJECTED`:
      return Object.assign({}, state, {
        isFetchingVehicle: false
      });
    case `${FETCH_VEHICLE}_FULFILLED`:
      return Object.assign({}, state, {
        isFetchingVehicle: false,
        ...action.payload
      });
    default:
      return state;
  }
};

export default dashboardReducer;

dashboardReducer.reducer = 'dashboard';
