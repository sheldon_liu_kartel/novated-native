import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Dashboard from './DashboardView';

import { actions as bindActions } from '../modules/dashboard';

import { selector } from '../../store/global/vehicles';

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(bindActions, dispatch)
});

const mapStateToProps = state => ({
  vehicle: selector.getSelectedVehicle(state),
  isFetchingVehicle: state.dashboard.isFetchingVehicle,
  budget: state.dashboard.budget,
  lease: state.dashboard.lease,
  claims: state.dashboard.claims,
  config: state.dashboard.config
});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
