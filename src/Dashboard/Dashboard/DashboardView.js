import React from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';
import styled from 'styled-components/native';

import {NitroText} from '../../../NitroNative';

const Header = styled.View`
  height: 40;
  flex-direction: row;
  align-content: center;
  background: ${props => props.theme.color.white};
  padding-left: 10;
  padding-right: 10;
  padding-top: 4;
  padding-bottom: 4;
`;
const WelcomeMessage = styled.Text`
  font-size: 10;
  flex: 4;
  align-self: center;
  color: ${props => props.theme.color.base1};
`;

const CarList = styled.View`
  background: ${props => props.theme.color.secondary};
  flex: 1;
  align-items: center;
  justify-content: center;
`;
const CarPlate = styled.Text`
  color: ${props => props.theme.color.white};
  font-size: 8;
`;

const CarSection = styled.View`
  height: 120;
  background: ${props => props.theme.color.secondary};
  flex-direction: row;
  align-items: center;
`;

const CarImage = styled.Image`
  margin-left: 20;
  width: 80;
  height: 80;
  background: ${props => props.theme.color.white};
`;

const CarInfo = styled.View`
  justify-content: center;
  margin-left: 20;
`;
const CarBrand = styled.Text`
  font-size: 14;
  font-weight: bold;
  color: ${props => props.theme.color.white};
`;

const CarModel = styled.Text`
  font-size: 14;
  color: ${props => props.theme.color.white};
`;


const VehiclePlate = styled.View`
  padding: 2px 8px;
  border: 1px solid #000000;
  border-radius: 4px;
  background: #ffe600;
`;

const Dashboard = ({ isFetchingVehicle, vehicle, budget, lease, claims, config }) => {

  return (
    <View>
      <CarSection>
        <CarImage
          resizeMode="contain"
          source={{
            uri: vehicle.url,
            method: 'GET'
          }}
        />
        <CarInfo>
          <CarBrand>{vehicle.make}</CarBrand>
          <CarModel>{vehicle.model}</CarModel>
          <VehiclePlate>
            <NitroText variant="base1" size="3" bold>
              {vehicle.registration}
            </NitroText>
          </VehiclePlate>
        </CarInfo>
      </CarSection>
      <Text>Test</Text>
    </View>
  );
};

export default Dashboard;
