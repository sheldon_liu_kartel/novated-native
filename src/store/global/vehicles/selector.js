import { createSelector } from 'reselect';

import { isEmpty } from 'lodash';

import { API_ROOT } from '../../../config';

const formatVehicleDetial = lease => {
  const {
    LeaseId: leaseId,
    Make: make,
    Model: model,
    RegistrationNumber: registration,
    VehicleImageUrl: url,
    BuildDate: year,
    Registered: registrationStatus
  } = lease;
  return {
    leaseId,
    make,
    model,
    registration,
    url: `${API_ROOT}/${url}`,
    year,
    registrationStatus
  };
};

export const getVehicleList = state => state.vehicles.vehicleList.map(vehicle => formatVehicleDetial(vehicle));

const getSelected = state => state.vehicles.selected;

export const getSelectedVehicle = createSelector(getSelected, getVehicleList, (selected, vehicleList) => {
  if (isEmpty(vehicleList)) {
    return {};
  }
  return selected ? vehicleList.find(v => v.leaseId === selected) : vehicleList[0];
});
