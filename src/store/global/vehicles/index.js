import * as selector from './selector';
import reducer, { actions } from './vehicles';

export { selector, actions };
export default reducer;
