import axios from 'axios';
import { API_ROOT } from '../../../config';

const endpoints = {
  vehicleList: `${API_ROOT}/leases`
};

// Dummy data
const vehicleList = [
  {
    LeaseId: 1,
    Make: 'Tesla',
    Model: 'Model X P100D',
    RegistrationNumber: 'KDM123',
    RegistrationStatus: true,
    VehicleImageUrl: 'https://fortunedotcom.files.wordpress.com/2016/05/tes-06-01-16-tesla-model-x-5.jpg'
  },
  {
    LeaseId: 2,
    Make: 'Toyota',
    Model: 'Corolla Sedan 2014',
    RegistrationNumber: 'LKF816',
    RegistrationStatus: false,
    VehicleImageUrl:
      'https://res.cloudinary.com/carsguide/image/upload/f_auto,fl_lossy,q_auto,t_cg_hero_low/v1/editorial/dp/images/uploads/Toyota-Corolla-sedan-%282%29-W.jpg'
  },
  {
    LeaseId: 3,
    Make: 'Volkswagen',
    Model: 'Golf Hatch 2009',
    RegistrationNumber: '1DUG998',
    RegistrationStatus: false,
    VehicleImageUrl: null
  }
];
function DelayPromise(delay) {
  // return a function that accepts a single variable
  return function(data) {
    // this function returns a promise.
    return new Promise(function(resolve, reject) {
      setTimeout(function() {
        // a promise that is resolved after "delay" milliseconds with the data provided
        resolve(data);
      }, delay);
    });
  };
}

const Api = {
  getVehicleList: () => {
    // const promise = new Promise((resolve, reject) => {
    //   resolve({
    //     data: vehicleList,
    //     status: 200,
    //     statusText: 'OK',
    //     headers: {}
    //   });
    // });
    // return promise.then(DelayPromise(1800)).then(data => data.data);
    // , headers: {'Cache-Control': 'max-age=foo'}
    return axios({method: 'get', url: endpoints.vehicleList}).then(({data}) => data.data);
  }
};

// ------------------------------------
// Constants
// ------------------------------------

const FETCH_VEHICLE_LIST = 'global/FETCH_VEHICLE_LIST';
const SELECT_VEHICLE = 'SELECT_VEHICLE';
// ------------------------------------
// Actions
// ------------------------------------

export const fetchVehicleList = () => ({
  type: FETCH_VEHICLE_LIST,
  payload: Api.getVehicleList()
});

export const selectVehicle = vehicle => ({
  type: SELECT_VEHICLE,
  payload: vehicle.leaseId
});

export const actions = {
  fetchVehicleList,
  selectVehicle
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  vehicleList: [],
  selected: '',
  isFetchingVehicleList: false
};

const vehiclesReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case `${FETCH_VEHICLE_LIST}_PENDING`:
      return Object.assign({}, state, {
        isFetchingVehicleList: true
      });
    case `${FETCH_VEHICLE_LIST}_REJECTED`:
      return Object.assign({}, state, {
        isFetchingVehicleList: false
      });
    case `${FETCH_VEHICLE_LIST}_FULFILLED`:
      return Object.assign({}, state, {
        isFetchingVehicleList: false,
        vehicleList: action.payload
      });
    case SELECT_VEHICLE:
      return Object.assign({}, state, {
        selected: action.payload
      });
    default:
      return state;
  }
};

export default vehiclesReducer;
