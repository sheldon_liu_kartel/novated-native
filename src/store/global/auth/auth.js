const initialState = {
  roles: [
    {
      label: 'Fleet Manager',
      value: 'fleet',
      site: 'fleet'
    },
    {
      label: 'Novated Lease',
      value: 'novated',
      site: 'novated'
    }
  ],
  isAuthenticated: true,
  userMustAcceptTerms: true,
  config: {},
  user: {
    username: 'David.Tanti@eclipx.com',
    firstName: 'David',
    lastName: 'Tanti'
  },
  connectionError: ''
};

const AUTH_SUCCESS = 'auth/AUTH_SUCCESS';

const auth = (state = initialState, action = {}) => {
  switch (action.type) {
    case AUTH_SUCCESS:
      return Object.assign({}, state, {
        isAuthenticated: true
      });
    default:
      return state;
  }
};

export default auth;
