import { createStore, applyMiddleware, combineReducers } from 'redux';

import auth from './global/auth';
import vehicles from './global/vehicles';

import dashboard from '../Dashboard/modules/dashboard'

import promiseMiddleware from 'redux-promise-middleware';

const rootReducer = combineReducers({
  auth,
  vehicles,
  dashboard
});

export default function configureStore() {
  let store = createStore(rootReducer, applyMiddleware(promiseMiddleware()));
  return store;
}
