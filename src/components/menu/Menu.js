import React from 'react';
import { View, Text } from 'react-native';

import PropTypes from 'prop-types';
import styled from 'styled-components/native';
// import { NitroIcon } from 'nitro-components';

import { Link } from 'react-router-native';

const MenuContainer = styled.View`

  height: 50;
  left: 0;
  right: 0;
  background: ${props => props.theme.color.white};
`;

const MenuList = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin: 0;
`;

const MenuListItem = styled(Link)`
  flex: 1;
  background: ${props => props.theme.color.secondary};
  align-items: center;
  padding-vertical: 20;
`;

const MenuItem = styled(Text)`
  color: ${props => props.theme.color.white};
`;

const Menu = ({menus}) => {
  return (
    <MenuContainer>
      <MenuList>
        {menus.map((menu, index) =>
          <MenuListItem key={`mobile-nav-${menu.title}`} underlayColor="#bbb" to={menu.route} exact index={index}>
            <MenuItem>{menu.title}</MenuItem>
          </MenuListItem>
        )}
      </MenuList>
    </MenuContainer>
  );
};
Menu.contextTypes = {
  router: PropTypes.object
};
Menu.propTypes = {
  menus: PropTypes.array
};

export default Menu;
