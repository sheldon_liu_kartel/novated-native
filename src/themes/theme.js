export default {
  // color
  primary: '#B9B309',

  secondary: '#00457C',

  support1: '#19DBC6',
  support2: '#0098C0',
  support3: '#16427c',

  arrow: '#00546B',

  // Budget ring use
  budget: '#19d9c5',

  underBudget1: '#0096c0',
  underBudget2: '#00546B',
  underBudgetBox: '#16427C',
  underBudgetVariant: '#00DBC5',

  overBudget1: '#ff3d00',
  overBudget2: '#982a0a',
  overBudgetBox: '#DC6B31',
  overBudgetVariant: '#FF5F0C',

  // used for inner elements-specific to theme
  theme: '#00457C'
};
