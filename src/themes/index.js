import theme from './theme';
import base from './base';

export default {
  ...base,
  color: { ...base.color, ...theme }
}
