export default {
  color: {
    base1: '#3e3b41',
    base2: '#707070',
    base3: '#bbbbbb',
    border: '#e6e6e6',
    success: '#1fa100',
    failure: '#ff0000',
    white: '#ffffff',
    black: '#000000',
    notification: '#FF632D',
    positive: '#74C400',
    negative: '#d24545'
  },

  size: {
    '1': '18',
    '2': '16',
    '3': '14',
    '4': '12',
    '5': '10'
  },

  font: {
    default: 'noto-sans-regular',
    bold: 'noto-sans-bold'
  },

  boxShadow: {
    '1': '0px 6px 10px 0px rgba(0, 0, 0, 0.15)',
    '2': '0 -3px 5px -1px rgba(0, 0, 0, .2)',
    '3': '0 3px 5px -1px rgba(0, 0, 0, .15)',
    filter: '0px 3px 8px -1px rgba(0, 0, 0, 0.2)'
  }
};
