import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Platform } from 'react-native';
import styled from 'styled-components/native';

import { connect } from 'react-redux';

import { Redirect } from 'react-router-native';

const Body = styled.View`
  flex: 1;
  padding-top: ${props => (Platform.OS === 'ios') ? 20 : 0};
`

const AuthenticatedRoutes = ({isAuthenticated, children}) => {
  return isAuthenticated
    ? <Body>{children}</Body>
    : <Redirect to="/login" />;
};


AuthenticatedRoutes.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired
};

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps)(AuthenticatedRoutes);
