import React from 'react';

import { NativeRouter, Route, Link, Switch } from 'react-router-native';

import AuthenticatedRoute from './AuthenticatedRoute';

// import configureStore from '../store';

// import PageLayout from 'layouts/PageLayout';

import Dashboard from '../Dashboard';
import Profile from '../Profile';

import VehicleView from '../global/VehicleView';
import PageLayout from '../layouts/PageLayout';

const appRouter = ({ store }) => {
  return (
    // <Switch>
    //   <Route
    //     path="/"
    //     component={props =>
    //       }
    //   />
    // </Switch>

    <AuthenticatedRoute>
      <PageLayout>
        <VehicleView>
          <Switch>
            <Route exact path="/" component={Dashboard} />
            <Route path="/account" component={Profile} />
          </Switch>
        </VehicleView>
      </PageLayout>
    </AuthenticatedRoute>
  );
};

export default appRouter;
