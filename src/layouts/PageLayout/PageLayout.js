import React, { Component } from 'react';
import { ScrollView, View, Text } from 'react-native';

import PropTypes from 'prop-types';
import styled from 'styled-components/native';

import Menu from '../../components/Menu';
import WelcomeHeader from '../../global/WelcomeHeader';

const BodyContainer = styled.View`
  flex: 1;
`;

const MENUS = [
  {
    title: 'Home',
    route: '/',
    icon: 'aperture'
  },
  {
    title: 'Budget',
    route: '/budget',
    icon: 'budget'
  },
  {
    title: 'Claim',
    route: '/claims',
    icon: 'claim'
  },
  {
    title: 'Lease',
    route: '/lease',
    icon: 'car'
  },
  {
    title: 'Account',
    route: '/account',
    icon: 'profile'
  }
];

class Layout extends Component {
  constructor(props) {
    super(props);
    this.state = { isMenuDisplay: true };
  }

  // componentDidMount() {
  //   window.addEventListener('scroll', this.hideBar);
  // }
  // componentWillUnmount() {
  //   window.removeEventListener('scroll', this.hideBar);
  // }

  hideBar = () => {
    // let { isMenuDisplay } = this.state;
    let displayMenu = true;

    const top = typeof window.scrollY === 'undefined' ? window.pageYOffset : window.scrollY;

    if (top > this.prev && top > 0) {
      displayMenu = false;
    }
    if (window.innerHeight + top + 70 >= document.body.offsetHeight) {
      displayMenu = true;
    }

    if (this.mobileMenu) {
      this.setState({
        isMenuDisplay: displayMenu
      });
    }
    this.prev = top;
  };

  render() {
    const { isMenuDisplay } = this.state;

    return (
      <BodyContainer>
        {/* <WelcomeHeader display="mobile" /> */}
        <ScrollView contentContainerStyle={{ paddingBottom: 100}}>
          <WelcomeHeader />
          {this.props.children}
        </ScrollView>
        <Menu menus={MENUS} isMenuDisplay={isMenuDisplay} />
      </BodyContainer>
    );
  }
}

export default Layout;
