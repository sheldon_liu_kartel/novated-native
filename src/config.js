// const DEV_SERVER_API = '//meldc1devtst1v:4000/api/novated';
// meldc1devtst1v
// 10.88.1.106
// const localhosts = ['localhost', '10.211.55.2', '10.88.73.56', 'meldc1dypdev1v'];
//
// const PHONE_IPS = ['10.10.88.121', '10.0.0.122'];
const DEV_PHONE_API = 'http://10.88.1.106:4000/api/novated';

const getAPIRoot = () => {
  // const { hostname, host } = window.location;
  //
  // for (const i in localhosts) {
  //   const localhost = localhosts[i];
  //   if (hostname.indexOf(localhost) > -1) {
  //     return DEV_SERVER_API;
  //   }
  // }
  // for (const i in PHONE_IPS) {
  //   const localhost = PHONE_IPS[i];
  //   if (hostname.indexOf(localhost) > -1) {
  //     return DEV_PHONE_API;
  //   }
  // }
  return DEV_PHONE_API;
  return `//${host}/api/novated`;
};

export const API_ROOT = getAPIRoot();

const SITENAMES = {
  fleetpartners: 'FleetPartners',
  fleetplus: 'FleetPlus',
  fleetsmart: 'FleetSmart'
};

// Set sitename at localhost
const LOCALHOST = 'fleetpartners';

const getSiteName = () => {
  let siteName = SITENAMES[LOCALHOST];

  // Object.keys(SITENAMES).map(key => {
  //   if (window.location.hostname.indexOf(key) > -1) {
  //     siteName = SITENAMES[key];
  //   }
  //   return null;
  // });
  return siteName;
};

export const SITE_NAME = getSiteName();
export default SITE_NAME;
